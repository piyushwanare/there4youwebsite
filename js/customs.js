
jQuery(function($) {

	"use strict";

		
		
		/**
		 * introLoader - Preloader
		 */
		$("#introLoader").introLoader({
			animation: {
					name: 'gifLoader',
					options: {
							ease: "easeInOutCirc",
							style: 'dark bubble',
							delayBefore: 500,
							delayAfter: 0,
							exitTime: 300
					}
			}
		});	

		
		
		/**
		 * Sticky Header
		 */
		$(".container-wrapper").waypoint(function() {
			$(".navbar").toggleClass("navbar-sticky-function");
			$(".navbar").toggleClass("navbar-sticky");
			return false;
		}, { offset: "-20px" });
		
		$('.interest-block label').click(function(){
			$(this).find(".back-color").toggleClass('click')
		})
		
		
		/**
		 * Main Menu Slide Down Effect
		 */
		$(".register .next").click(function(){
			var next=2
			$(".details-block .required").each(function(){
	             if($(this).val() == ''){
	             	next=1
	             	alert("please fill all the required fields")
	             	$(this).focus()

	             	return false
	             }
	             else{
	             	next=0;
	             }
	        });
	        if(next==0){
	        	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z])+\.([A-Za-z]{2,4})$/;

		        if (reg.test($('.email').val()) == false) 
		        {
		            alert('Invalid email address');
		            return false;
		        }
		        else{
		        	next=4
		        }

	        }
	        if(next==4)
	        {
				$('.interest').addClass('orange')
				$('.interest').removeClass('dark')
				$(".detail").addClass('dark')
				$(".detail").removeClass('orange')
				$(".register .details-block").addClass('hide')
				$(".register .interest-block").removeClass('hide')
			}
		});
		$(".register .interest").click(function(){
			var next=2
			$(".details-block .required").each(function(){
	             if($(this).val() == ''){
	             	next=1
	             	alert("please fill all the required fields")
	             	$(this).focus()
	             	return false
	             }
	             else{
	             	next=0;
	             }
	        });
	        if(next==0){
	        	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z])+\.([A-Za-z]{2,4})$/;

		        if (reg.test($('.email').val()) == false) 
		        {
		            alert('Invalid email address');
		            return false;
		        }
		        else{
		        	next=4
		        }

	        }
	        if(next==4){
	        	$(this).addClass('orange')
				$(this).removeClass('dark')
				$(".detail").addClass('dark')
				$(".detail").removeClass('orange')
				$(".register .details-block").addClass('hide')
				$(".register .interest-block").removeClass('hide')
			}
		});
		$(".register .detail").click(function(){
			$(this).removeClass('dark')
			$('.interest').addClass('dark')
			$('.interest').removeClass('orange')
			$(this).addClass('orange')
			$(".register .details-block").removeClass('hide')
			$(".register .interest-block").addClass('hide')
		});
		 
		// Mouse-enter dropdown
		$('#navbar li').on("mouseenter", function() {
				$(this).find('ul').first().stop(true, true).delay(350).slideDown(500, 'easeInOutQuad');
		});

		// Mouse-leave dropdown
		$('#navbar li').on("mouseleave", function() {
				$(this).find('ul').first().stop(true, true).delay(100).slideUp(150, 'easeInOutQuad');
		});
		
		
		
		/**
		 * Effect to Bootstrap Dropdown
		 */
		$('.bt-dropdown-click').on('show.bs.dropdown', function(e) {   
			$(this).find('.dropdown-menu').first().stop(true, true).slideDown(500, 'easeInOutQuad'); 
		}); 
		$('.bt-dropdown-click').on('hide.bs.dropdown', function(e) { 
			$(this).find('.dropdown-menu').first().stop(true, true).slideUp(250, 'easeInOutQuad'); 
		});
		
		
		
		/**
		 * Icon Change on Collapse
		 */
		$('.collapse.in').prev('.panel-heading').addClass('active');
		$('.bootstarp-accordion, .bootstarp-toggle').on('show.bs.collapse', function(a) {
			$(a.target).prev('.panel-heading').addClass('active');
		})
		.on('hide.bs.collapse', function(a) {
			$(a.target).prev('.panel-heading').removeClass('active');
		});
		
		
		
		/**
		 * Slicknav - a Mobile Menu
		 */
		$('#responsive-menu').slicknav({
			duration: 300,
			easingOpen: 'easeInExpo',
			easingClose: 'easeOutExpo',
			closedSymbol: '<i class="fa fa-plus"></i>',
			openedSymbol: '<i class="fa fa-minus"></i>',
			prependTo: '#slicknav-mobile',
			allowParentLinks: true,
			label:"" 
		});
		
		
		
		/**
		 * Smooth scroll to anchor
		 */
		$('a.anchor[href*=#]:not([href=#])').on("click",function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: (target.offset().top - 120) // 70px offset for navbar menu
					}, 1000);
					return false;
				}
			}
		});
		
		
		
		/**
		 * Sign-in Modal
		 */
		var $formLogin = $('#login-form');
		var $formLost = $('#lost-form');
		var $formRegister = $('#register-form');
		var $divForms = $('#modal-login-form-wrapper');
		var $modalAnimateTime = 300;
		
		$('#login_register_btn').on("click", function () { modalAnimate($formLogin, $formRegister) });
		$('#register_login_btn').on("click", function () { modalAnimate($formRegister, $formLogin); });
		$('#login_lost_btn').on("click", function () { modalAnimate($formLogin, $formLost); });
		$('#lost_login_btn').on("click", function () { modalAnimate($formLost, $formLogin); });
		$('#lost_register_btn').on("click", function () { modalAnimate($formLost, $formRegister); });
		
		function modalAnimate ($oldForm, $newForm) {
			var $oldH = $oldForm.height();
			var $newH = $newForm.height();
			$divForms.css("height",$oldH);
			$oldForm.fadeToggle($modalAnimateTime, function(){
				$divForms.animate({height: $newH}, $modalAnimateTime, function(){
					$newForm.fadeToggle($modalAnimateTime);
				});
			});
		}

		
		
		/**
		 * select2 - custom select
		 */
		$(".select2-single").select2({allowClear: true});
		$(".select2-no-search").select2({dropdownCssClass : 'select2-no-search',allowClear: true});
		$(".select2-multi").select2({});
		
		
		
		/**
		 * Show more-less button
		 */
		$('.btn-more-less').on("click",function(){
			$(this).text(function(i,old){
				return old=='Show more' ?  'Show less' : 'Show more';
			});
		});

		
		
		/**
		 *  Arrow for Menu has sub-menu
		 */
		$(".navbar-arrow > ul > li").has("ul").children("a").append("<i class='arrow-indicator fa fa-angle-down'></i>");
		$(".navbar-arrow ul ul > li").has("ul").children("a").append("<i class='arrow-indicator fa fa-angle-right'></i>");
		
		
		
		/**
		 *  Placeholder
		 */
		$("input, textarea").placeholder();

		/**
		 * responsivegrid - layout grid
		 */
		$('.grid').responsivegrid({
			gutter : '0',
			itemSelector : '.grid-item',
			'breakpoints': {
				'desktop' : {
					'range' : '1200-',
					'options' : {
						'column' : 20,
					}
				},
				'tablet-landscape' : {
					'range' : '1000-1200',
					'options' : {
						'column' : 20,
					}
				},
				'tablet-portrate' : {
					'range' : '767-1000',
					'options' : {
						'column' : 20,
					}
				},
				'mobile-landscape' : {
					'range' : '-767',
					'options' : {
						'column' : 10,
					}
				},
				'mobile-portrate' : {
					'range' : '-479',
					'options' : {
						'column' : 10,
					}
				},
			}
		});
		
		
		
		/**
		 * Payment Option
		 */
		$("div.payment-option-form").hide();
		$("input[name$='payments']").on("click",function() {
				var test = $(this).val();
				$("div.payment-option-form").hide();
				$("#" + test).show();
		});
		
		
		
		/**
		 * Raty - Rating Star
		 */
		$.fn.raty.defaults.path = 'images/raty';
		
		// Default size star 
		$('.star-rating').raty({
			round : { down: .2, full: .6, up: .8 },
			half: true,
			space: false,
			score: function() {
				return $(this).attr('data-rating-score');
			}
		});
		
		// Read onlu default size star
		$('.star-rating-read-only').raty({
			readOnly: true,
			round : { down: .2, full: .6, up: .8 },
			half: true,
			space: false,
			score: function() {
				return $(this).attr('data-rating-score');
			}
		});
		
		// Smaller size star
		$('.star-rating-12px').raty({
			path: 'images/raty',
			starHalf: 'star-half-sm.png',
			starOff: 'star-off-sm.png',
			starOn: 'star-on-sm.png',
			readOnly: true,
			round : { down: .2, full: .6, up: .8 },
			half: true,
			space: false,
			score: function() {
				return $(this).attr('data-rating-score');
			}
		});
		
		// White color default size star
		$('.star-rating-white').raty({
			path: 'images/raty',
			starHalf: 'star-half-white.png',
			starOff: 'star-off-white.png',
			starOn: 'star-on-white.png',
			readOnly: true,
			round : { down: .2, full: .6, up: .8 },
			half: true,
			space: false,
			score: function() {
				return $(this).attr('data-rating-score');
			}
		});
		
		/*banner*/
		 $('.banner-section').mouseenter(function(){
		 	var clas=$(this).attr('class')
		 $(this).addClass('hii')	 
		 $(this).addClass('hover')
		 $('.Banners-on div').each(function(){
		 	if ($(this).attr('class')== clas) {
		 	}
		 })
        
	    });
	    $('.banner-section').mouseleave(function(){
	    	$(this).removeClass('hii')
	    	$(this).removeClass('hover')
	    	$('.zoom').removeClass('zoom')
	    	$(this).css("background-size","center")
	        $(this).find('.onhover').removeClass('fadeInDown')
	        $(this).find('h5').removeClass('fadeOutDown')
	    });

	    $('#login').click(function(){
	        $('.login').removeClass('fadeOutDown')
	        $('.login').addClass('appear')
	        $('.login').addClass('fadeInDown')
	    });
	    $('.login .btn-danger').click(function(){
	        $('.login').addClass('fadeOutDown')
	        $('.login').removeClass('fadeInDown')
	        $('.login').removeClass('appear')
	    });
	    $('.icon-side').click(function(){
	        $('.icon-side').addClass('side-arrow-inactive')
	        $('.fa-angle-double-down').removeClass('side-arrow-active')
	        $(this).removeClass('side-arrow-inactive')
	        $(this).addClass('side-arrow-active')
	        $('.side-arrow-inactive').removeClass('side-arrow-active')
	        $('.side-blog-title h5').removeClass('active-title')
	        $("h5[for='" + $(this).find('svg').attr('id') + "']").addClass('active-title')
	        $('.side-blog-title-data').html($(this).parent().find('.this-data').html())
	    });
	    console.log(sessionStorage.getItem('onetime'))
	    if (sessionStorage.getItem('onetime')=='yes') {

	    }
	    else{
	    	$(".onetime").modal('show');
	    	sessionStorage.setItem('onetime','yes')	
	    }


		

		
		/**
		 * readmore - read more/less
		 */
		$('.read-more-div').readmore({
			speed: 220,
			moreLink: '<a href="#" class="read-more-div-open">Read More</a>',
			lessLink: '<a href="#" class="read-more-div-close">Read less</a>',
			collapsedHeight: 45,
			heightMargin: 25
		});

		
		
		/**
		 * ionRangeSlider - range slider
		 */
		 
		 // Price Range Slider
		$("#price_range").ionRangeSlider({
			type: "double",
			grid: true,
			min: 0,
			max: 1000,
			from: 200,
			to: 800,
			prefix: "$"
		});
		
		// Star Range Slider
		$("#star_range").ionRangeSlider({
			type: "double",
			grid: false,
			from: 1,
			to: 2,
			values: [
				"<i class='fa fa-star'></i>", 
				"<i class='fa fa-star'></i> <i class='fa fa-star'></i>",
				"<i class='fa fa-star'></i> <i class='fa fa-star'></i> <i class='fa fa-star'></i>", 
				"<i class='fa fa-star'></i> <i class='fa fa-star'></i> <i class='fa fa-star'></i> <i class='fa fa-star'></i>",
				"<i class='fa fa-star'></i> <i class='fa fa-star'></i> <i class='fa fa-star'></i> <i class='fa fa-star'></i> <i class='fa fa-star'></i>" 
			]
		});

		

		/**
		 * slick
		 */
		$('.gallery-slideshow').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			speed: 500,
			arrows: true,
			fade: true,
			asNavFor: '.gallery-nav'
		});
		$('.gallery-nav').slick({
			slidesToShow: 7,
			slidesToScroll: 1,
			speed: 500,
			asNavFor: '.gallery-slideshow',
			dots: false,
			centerMode: true,
			focusOnSelect: true,
			infinite: true,
			responsive: [
				{
				breakpoint: 1199,
				settings: {
					slidesToShow: 7,
					}
				}, 
				{
				breakpoint: 991,
				settings: {
					slidesToShow: 5,
					}
				}, 
				{
				breakpoint: 767,
				settings: {
					slidesToShow: 5,
					}
				}, 
				{
				breakpoint: 480,
				settings: {
					slidesToShow: 3,
					}
				}
			]
		});

		$(window).bind('scroll', function() {
		    var currentTop = $(window).scrollTop();
		    var elems = $('.scrollspy');
		    elems.each(function(index){
		      var elemTop 	= $(this).offset().top;
		      var elemBottom 	= elemTop + $(this).height();
		      if(currentTop >= elemTop && currentTop <= elemBottom){
		        var id 		= $(this).attr('id');
		        var navElem = $('a[href="#' + id+ '"]');
		    navElem.parent().addClass('active').siblings().removeClass( 'active' );
		      }
		    })
		}); 

		var backTop=document.getElementsByClassName('js-cd-top')[0],offset=300,offsetOpacity=1200,scrollDuration=700
scrolling=false;if(backTop){window.addEventListener("scroll",function(event){if(!scrolling){scrolling=true;(!window.requestAnimationFrame)?setTimeout(checkBackToTop,250):window.requestAnimationFrame(checkBackToTop);}});backTop.addEventListener('click',function(event){event.preventDefault();(!window.requestAnimationFrame)?window.scrollTo(0,0):scrollTop(scrollDuration);});}
function checkBackToTop(){var windowTop=window.scrollY||document.documentElement.scrollTop;(windowTop>offset)?addClass(backTop,'cd-top--show'):removeClass(backTop,'cd-top--show','cd-top--fade-out');(windowTop>offsetOpacity)&&addClass(backTop,'cd-top--fade-out');scrolling=false;}
function scrollTop(duration){var start=window.scrollY||document.documentElement.scrollTop,currentTime=null;var animateScroll=function(timestamp){if(!currentTime)currentTime=timestamp;var progress=timestamp-currentTime;var val=Math.max(Math.easeInOutQuad(progress,start,-start,duration),0);window.scrollTo(0,val);if(progress<duration){window.requestAnimationFrame(animateScroll);}};window.requestAnimationFrame(animateScroll);}
Math.easeInOutQuad=function(t,b,c,d){t/=d/2;if(t<1)return c/2*t*t+b;t--;return-c/2*(t*(t-2)-1)+b;};function hasClass(el,className){if(el.classList)return el.classList.contains(className);else return!!el.className.match(new RegExp('(\\s|^)'+className+'(\\s|$)'));}
function addClass(el,className){var classList=className.split(' ');if(el.classList)el.classList.add(classList[0]);else if(!hasClass(el,classList[0]))el.className+=" "+classList[0];if(classList.length>1)addClass(el,classList.slice(1).join(' '));}
function removeClass(el,className){var classList=className.split(' ');if(el.classList)el.classList.remove(classList[0]);else if(hasClass(el,classList[0])){var reg=new RegExp('(\\s|^)'+classList[0]+'(\\s|$)');el.className=el.className.replace(reg,' ');}
if(classList.length>1)removeClass(el,classList.slice(1).join(' '));}
		/**
		 * Back To Top
		 */
		$(window).scroll(function(){
			if($(window).scrollTop() > 500){
				$("#back-to-top").fadeIn(200);
			} else if ($(window).scrollTop() < 500){
				$("#back-to-top").fadeOut(200);
			}
		});
		$("#back-to-top").on('click', function(event) {

		    var target = $(this.getAttribute('href'));

		    if( target.length ) {
		        event.preventDefault();
		        $('html, body').stop().animate({
		            scrollTop: target.offset().top
		        }, 1000);
		    }

		});
		
		
		
		/**
		 * Instagram
		 */
		function createPhotoElement(photo) {
			var innerHtml = $('<img>')
			.addClass('instagram-image')
			.attr('src', photo.images.thumbnail.url);

			innerHtml = $('<a>')
			.attr('target', '_blank')
			.attr('href', photo.link)
			.append(innerHtml);

			return $('<div>')
			.addClass('instagram-placeholder')
			.attr('id', photo.id)
			.append(innerHtml);
		}

		function didLoadInstagram(event, response) {
			var that = this;
			$.each(response.data, function(i, photo) {
			$(that).append(createPhotoElement(photo));
			});
		}

		$(document).ready(function() {
			
			$('#instagram').on('didLoadInstagram', didLoadInstagram);
			$('#instagram').instagram({
				count: 20,
				userId: 3301700665,
				accessToken: '3301700665.4445ec5.c3ba39ad7828412286c1563cac3f594b'
			});

		});

})(jQuery);
